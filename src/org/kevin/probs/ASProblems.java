package org.kevin.probs;

import java.util.HashSet;
import java.util.Set;

/**
 * Problems relating to Arrays and Strings
 * @author Kevin
 */
public class ASProblems {
    public static String reverseString(String s) {
        char[] string = s.toCharArray();
        StringBuilder reverse = new StringBuilder();
        for (int i = 0; i < string.length; i++) {
            reverse.append(string[string.length - i - 1]);
        }
        return reverse.toString();
    }

    public static String reverseVowels(String s) {
        char[] string = s.toCharArray();
        Set<Character> charSet = new HashSet<>();
        charSet.add('a');
        charSet.add('e');
        charSet.add('i');
        charSet.add('o');
        charSet.add('u');
        charSet.add('A');
        charSet.add('E');
        charSet.add('I');
        charSet.add('O');
        charSet.add('U');
        for (int i = 0, j = string.length - 1; i < j; ) {
            if (!charSet.contains(string[i])) {
                i++;
                continue;
            }
            if (!charSet.contains(string[j])) {
                j--;
                continue;
            }
            char temp = string[i];
            string[i] = string[j];
            string[j] = temp;
            i++;
            j--;
        }
        return String.valueOf(string);
    }

    public static int strToInt(String s) {
        int i = 0, n = 0;
        boolean negative = false;
        int len = s.length();
        if (s.charAt(0) == '-') {
            negative = true;
            i++;
        }
        while (i < len) {
            n *= 10;
            n += s.charAt(i++) - '0';
        }
        if (negative)
            n = -n;
        return n;
    }

    /**
     * Requires a temporary char array of size 11 (max digits in an integer)
     * @param n
     * @return
     */
    public static String intToStr(int n) {
        boolean negative = false;
        int i = 0;
        if (n < 0) {
            negative = true;
            n = -n;
        }
        char[] temp = new char[11];
        do {
            temp[i++] = (char)((n % 10) + '0');
            n /= 10;
        } while (n > 0);
        StringBuilder s = new StringBuilder();
        if (negative) s.append('-');
        while (i > 0)
            s.append(temp[--i]);
        return s.toString();
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        //System.out.println(ASProblems.strToInt("152"));
        //System.out.println(ASProblems.intToStr(0));
        //System.out.println(reverseString("hello"));
        System.out.println(reverseVowels("leEtcOde"));
        System.out.println((System.nanoTime() - start) / 1000000f + " ms");
    }
}
