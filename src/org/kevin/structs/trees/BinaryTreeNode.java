package org.kevin.structs.trees;

/**
 * @author Kevin
 */
public class BinaryTreeNode<T extends Comparable> extends TreeNode<T> {
    public BinaryTreeNode(BinaryTreeNode<T> parent, BinaryTreeNode<T> left, BinaryTreeNode<T> right, T data) {
        super(parent, new BinaryTreeNode[]{left, right}, data);
    }

    public void setLeft(BinaryTreeNode<T> left) {
        if (left != null) left.setParent(this);
        setChildren(new BinaryTreeNode[]{left, getRight()});
    }

    public void setRight(BinaryTreeNode<T> right) {
        if (right != null) right.setParent(this);
        setChildren(new BinaryTreeNode[]{getLeft(), right});
    }

    public BinaryTreeNode<T> getLeft() { return (BinaryTreeNode<T>)getChildByIndex(0); }
    public BinaryTreeNode<T> getRight() { return (BinaryTreeNode<T>)getChildByIndex(1); }
}
