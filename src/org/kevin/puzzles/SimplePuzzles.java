package org.kevin.puzzles;

public class SimplePuzzles {

    public boolean canWinNim(int n) {
        return n % 4 != 0;
    }

    public int addDigits(int num) {
        if (num < 10) return num;

        int newNum = 0;
        while (num != 0) {
            int rem = num % 10;
            newNum += rem;
            num -= rem;
            num /= 10;
        }

        return addDigits(newNum);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public int maxDepth(TreeNode root) {
        if (root == null) return 0;

        int leftDepth = 1 + maxDepth(root.left);
        int rightDepth = 1 + maxDepth(root.right);

        return leftDepth > rightDepth ? leftDepth : rightDepth;
    }

    public boolean isBalanced(TreeNode root) {
        if (root == null) return true;

        boolean balance = Math.abs(maxDepth(root.left) - maxDepth(root.right)) <= 1;

        return balance && isBalanced(root.left) && isBalanced(root.right);
    }

    public static void main(String[] args) {
        SimplePuzzles puzzles = new SimplePuzzles();

        System.out.println(puzzles.canWinNim(4));
        System.out.println(puzzles.canWinNim(5));
        System.out.println(puzzles.canWinNim(6));
        System.out.println(puzzles.canWinNim(7));
        System.out.println(puzzles.canWinNim(8));

        System.out.println(puzzles.addDigits(338));
    }
}
