package org.kevin.probs;

/**
 * Problems designed specifically to deal with Recursion
 * @author Kevin
 */
public class RecursionProblems {

    public static int binarySearch(int[] array, int target) {
        return binarySearch(array, target, 0, array.length-1);
    }

    /**
     * Recursive binary search through a sorted array to obtain the index of a given target value.
     * Time complexity O(log(n)).
     * @param array
     * @param target
     * @param lower
     * @param upper
     * @return
     */
    private static int binarySearch(int[] array, int target, int lower, int upper) {
        int range = upper - lower;
        int center = range/2 + lower;
        if (target == array[center]) {
            return center;
        } else if (target > array[center]) {
            return binarySearch(array, target, center+1, upper);
        } else {
            return binarySearch(array, target, lower, center-1);
        }
    }

    /**
     * Iterative binary search through a sorted array to obtain the index of a given target value. The iterative
     * method will provide a more efficient solution for larger search spaces. Time complexity O(log(n)).
     * @param array
     * @param target
     * @return
     */
    public static int iterBinarySearch(int[] array, int target) {
        int lower = 0, upper = array.length-1;
        int range, center;

        while (lower <= upper) {
            range = upper - lower;
            center = range/2 + lower;
            if (target == array[center]) {
                return center;
            } else if (target > array[center]) {
                lower = center + 1;
            } else {
                upper = center - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] sortedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        System.out.println(RecursionProblems.iterBinarySearch(sortedArray, 9));
    }
}
