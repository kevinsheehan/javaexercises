package org.kevin.structs.map;

import org.kevin.structs.LinkedListNode;

/**
 * @author Kevin
 */
public class HashMap<K, V> {
    private int DEFAULT_CAPACITY = 16;
    private int size = DEFAULT_CAPACITY;
    private LinkedListNode<MapEntry<K, V>>[] entries = new LinkedListNode[DEFAULT_CAPACITY];

    /**
     * Insertion is O(1). Lookup is constant time and we place at the start of the lists which is also constant.
     * @param key
     * @param value
     */
    public void put(K key, V value) {
        int index = hash(key);
        LinkedListNode<MapEntry<K, V>> entry = new LinkedListNode<>(new MapEntry<>(key, value));
        if (entries[index] == null) {
            entries[index] = entry;
        } else {
            entry.setNext(entries[index]);
            entries[index] = entry;
        }
    }

    /**
     * Lookup is O(1) potentially. With larger array and less chaining. More chaining causes lookup to approach O(n).
     * @param key
     * @return
     */
    public V get(K key) {
        int index = hash(key);
        LinkedListNode<MapEntry<K, V>> entry = entries[index];
        if (entry.getData().getKey() == key) {
            return entry.getData().getValue();
        }
        while (entry.getNext() != null) {
            if (entry.getData().getKey() == key) {
                return entry.getData().getValue();
            } else {
                entry = entry.getNext();
            }
        }
        return null;
    }

    public int hash(K key) {
        return key.hashCode() % size;
    }
}
