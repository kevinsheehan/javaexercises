package org.kevin.structs.graphs;

import java.util.ArrayList;

/**
 * @author Kevin
 */
public class GraphNode {
    private ArrayList<Edge> edges;

    public ArrayList<Edge> getEdges() { return edges; }
}
