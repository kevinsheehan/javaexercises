package org.kevin.structs;

/**
 * A singly linked list implementation for use in problem solutions
 * @author Kevin
 */
public class LinkedListNode<T> {
    private LinkedListNode<T> next;
    private T data;

    public LinkedListNode(T data) {
        this.data = data;
        next = null;
    }

    public T getData() {
        return data;
    }

    public LinkedListNode<T> getNext() {
        return next;
    }

    public LinkedListNode<T> setNext(LinkedListNode<T> next) {
        this.next = next;
        return next;
    }
}
