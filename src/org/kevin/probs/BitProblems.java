package org.kevin.probs;

/**
 * @author Kevin
 */
public class BitProblems {

    public static int numOnesInBinary(int number) {
        int count = 0;
        while (number != 0) {
            count += (number & 1) == 1 ? 1 : 0;
            number >>>= 1;
        }
        return count;
    }

    public static boolean highestOrderBitSet(byte number) {
        return (number & 1<<7) != 0;
    }

    public static boolean isPalindrome(int number) {
        int other = 0;
        for (int i = 0; i < 16; i++) {
            other <<= 1;
            if ((number & 1) == 1) {
                other += 1;
            }
            number >>>= 1;
            if (number < other) {
                break;
            }
        }
        return number == other;
        //return number == Integer.reverse(number);
    }

    public static void main(String[] argv) {
        System.out.println(BitProblems.numOnesInBinary(10));
        System.out.println(BitProblems.highestOrderBitSet((byte)0b10100000));
        System.out.println(BitProblems.isPalindrome(0b10100000000000000000000000000101));
    }
}
