package org.kevin.probs;

import org.kevin.structs.LLHeadTail;
import org.kevin.structs.LinkedListNode;

/**
 * Solution to obtaining the Mth element from the end of a Linked List
 * The time complexity of this solution is O(n)
 * The space complexity is minimal because we only track two nodes at any time
 * @author Kevin
 */
public class MthNodeLL {
    public static <T> T retrieveM(LLHeadTail<T> list, int m) {
        LinkedListNode<T> mNode = list.getElementAtIndex(0);
        if (mNode.getNext() == mNode) { // Only element in our list
            if (m == 0) return mNode.getData();
            else return null;
        }
        LinkedListNode<T> tailNode = mNode;
        int dist = 0;
        while (dist != m) {
            if (tailNode.getNext() != null) {
                tailNode = tailNode.getNext();
                dist++;
            } else {
                return null;
            }
        }
        while (tailNode.getNext() != null) {
            tailNode = tailNode.getNext();
            mNode = mNode.getNext();
        }
        return mNode.getData();
    }

    public static void main(String[] args) {
        Integer[] ints = { 0, 132, 14, 10, 5, 2, 68};
        LLHeadTail<Integer> list = new LLHeadTail<>();
        for (Integer i : ints) {
            list.insertAfter(null, i);
        }
        System.out.println(MthNodeLL.retrieveM(list, 0));
        System.out.println(MthNodeLL.retrieveM(list, 2));
        System.out.println(MthNodeLL.retrieveM(list, 4));
    }
}
