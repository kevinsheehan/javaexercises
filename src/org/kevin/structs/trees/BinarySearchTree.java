package org.kevin.structs.trees;

/**
 * @author Kevin
 */
public class BinarySearchTree<T extends Comparable> extends BinaryTreeNode<T> {
    public BinarySearchTree(BinarySearchTree<T> parent, BinarySearchTree<T> left, BinarySearchTree<T> right, T data) {
        super(parent, left, right, data);
    }

    /**
     * Insertion on binary search tree has a best-case of O(log{n}} and worst case of O(n)
     * @param data
     */
    public void insert(T data) {
        if (data.compareTo(getData()) < 0) {
            if (getLeft() == null) setLeft(new BinarySearchTree<>(this, null, null, data));
            else ((BinarySearchTree<T>)getLeft()).insert(data);
        } else {
            if (getRight() == null) setRight(new BinarySearchTree<>(this, null, null, data));
            else ((BinarySearchTree<T>)getRight()).insert(data);
        }
    }

    /**
     * Rotate Right is used for tree balancing while maintaining the requirements of a BST in O(1) time
     * @return New root (parent) of this tree
     */
    public BinaryTreeNode<T> rotateRight() {
        BinaryTreeNode<T> root = getLeft();
        setLeft(root.getRight());
        root.setRight(this);
        return root;
    }

    /**
     * Rotate Left is used for tree balancing while maintaining the requirements of a BST in O(1) time
     * @return New root (parent) of this tree
     */
    public BinaryTreeNode<T> rotateLeft() {
        BinaryTreeNode<T> root = getRight();
        setRight(root.getLeft());
        root.setLeft(this);
        return root;
    }
}
