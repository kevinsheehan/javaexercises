package org.kevin.puzzles;

import java.io.*;

/**
 * User: Kevin Sheehan
 * Date: 11/9/13
 */
public class Triangle {
    private int[][] triangle;
    private int size;

    /**
     * This size variable is implemented here just for simplicity sake. This way I don't have to size down the array.
     * @param triangle
     * @param size
     */
    public Triangle(int[][] triangle, int size) {
        this.triangle = triangle;
        this.size = size;
    }

    /**
     * This problem can be handled simply using a recursive method for smaller triangles. When the triangle gets much
     * larger then the recursive method requires way too much overhead and takes far too long.
     * @param row The row that we are currently in
     * @param index The index of our position in the current row
     * @return Recursively return the largest sub path
     */
    public int recursiveSolve(int row, int index) {
        if (row == size - 1) return triangle[row][index];
        return triangle[row][index] + Math.max(recursiveSolve(row + 1, index), recursiveSolve(row + 1, index + 1));
    }

    /**
     * A bottom up approach.
     * @return Largest possible sum
     */
    public int solve() {
        // We want to start at the row that is second from the bottom
        for (int i = size - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                triangle[i][j] += Math.max(triangle[i+1][j], triangle[i+1][j+1]);
            }
        }
        return triangle[0][0];
    }

    public static void main(String[] args) {
        try {
            //load in triangle to 2D array
            BufferedReader br = new BufferedReader(new FileReader(args[0]));
            String line;
            int[][] triangle = new int[100][100];
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] tokens = line.trim().split("\\s+");
                for (int i = 0; i < index + 1; i++) {
                    triangle[index][i] = Integer.parseInt(tokens[i]);
                }
                index++;
            }
            Triangle solver = new Triangle(triangle, index);
            System.out.println(solver.solve());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}