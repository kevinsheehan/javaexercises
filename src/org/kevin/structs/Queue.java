package org.kevin.structs;

/**
 * @author Kevin
 */
public class Queue<T> {
    private LinkedListNode<T> first;
    private LinkedListNode<T> last;

    public Queue() {}

    public void add(T node) {
        LinkedListNode<T> temp = last;
        last = new LinkedListNode<>(node);
        if (first == null) first = last;
        else temp.setNext(last);
    }
    public T remove() {
        if (first == null) throw new NullPointerException();
        T temp = first.getData();
        if (first == last) {
            first = last = null;
        } else {
            first = first.getNext();
        }
        return temp;
    }

    public boolean isEmpty() {
        return first == null;
    }
}
