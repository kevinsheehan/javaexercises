package org.kevin.probs;

import org.kevin.structs.Queue;
import org.kevin.structs.graphs.Edge;
import org.kevin.structs.graphs.GraphNode;

/**
 * @author Kevin
 */
public class GraphProblems {

    public static int sixDegrees(GraphNode start) {
        Queue<GraphNode> queue = new Queue<>();
        queue.add(start);
        GraphNode current;
        while ((current = queue.remove()) != null) {
            for (Edge edge : current.getEdges()) {
                GraphNode other = edge.getNode1() == current ? edge.getNode1() : edge.getNode2();
                if (/* node unvisited */ true) {
                    //other.setVisited = true;
                    queue.add(other);
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        //
    }
}
