package org.kevin.probs;

public class SortingProblems {
    /**
     * leetcode.com #88
     * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
     *
     * Note:
     * You may assume that nums1 has enough space (size that is greater or equal to m + n)
     * to hold additional elements from nums2. The number of elements initialized in
     * nums1 and nums2 are m and n respectively.
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int destIdx = m + n - 1;
        m--;
        n--;
        while (m >= 0 && n >= 0) {
            if (nums1[m] >= nums2[n]) {
                nums1[destIdx--] = nums1[m--];
            } else {
                nums1[destIdx--] = nums2[n--];
            }
        }
        // Populate the remaining values
        while (m >= 0) {
            nums1[destIdx--] = nums1[m--];
        }
        while (n >= 0) {
            nums1[destIdx--] = nums2[n--];
        }
    }

    /**
     * leetcode.com #21
     * Merge two sorted linked lists and return it as a new list. The new list
     * should be made by splicing together the nodes of the first two lists.
     */
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode ret = null;
        ListNode curr = null;
        ListNode temp;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                if (ret != null) {
                    temp = new ListNode(l1.val);
                    curr.next = temp;
                    curr = temp;

                    if (ret.next == null) {
                        ret.next = curr;
                    }
                } else {
                    ret = curr = new ListNode(l1.val);
                }
                l1 = l1.next;
            } else {
                if (ret != null) {
                    temp = new ListNode(l2.val);
                    curr.next = temp;
                    curr = temp;

                    if (ret.next == null) {
                        ret.next = curr;
                    }
                } else {
                    ret = curr = new ListNode(l2.val);
                }
                l2 = l2.next;
            }
        }

        while (l1 != null) {
            if (ret != null) {
                temp = new ListNode(l1.val);
                curr.next = temp;
                curr = temp;

                if (ret.next == null) {
                    ret.next = curr;
                }
            } else {
                ret = curr = new ListNode(l1.val);
            }
            l1 = l1.next;
        }

        while (l2 != null) {
            if (ret != null) {
                temp = new ListNode(l2.val);
                curr.next = temp;
                curr = temp;

                if (ret.next == null) {
                    ret.next = curr;
                }
            } else {
                ret = curr = new ListNode(l2.val);
            }
            l2 = l2.next;
        }

        return ret;
    }

    public static ListNode mergeTwoListsRecursive(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    public static void main(String[] argv) {
        /*int[] nums1 = {1, 3, 4, 0, 0};
        int[] nums2 = {2, 5};
        int m = 3;
        int n = 2;
        merge(nums1, m, nums2, n);
        System.out.printf(Arrays.toString(nums1));*/

        ListNode list1 = new ListNode(-4);
        ListNode list1Curr = new ListNode(-2);
        list1.next = list1Curr;
        ListNode list2 = new ListNode(-3);
        ListNode list2Curr = new ListNode(-1);
        list2.next = list2Curr;
        for (int i = 0; i < 10; i++) {
            list1Curr.next = new ListNode(i++);
            list1Curr = list1Curr.next;
            list2Curr.next = new ListNode(i);
            list2Curr = list2Curr.next;
        }
        ListNode sorted = mergeTwoLists(list1, list2);
        while (sorted != null) {
            System.out.printf(String.valueOf(sorted.val));
            sorted = sorted.next;
        }
        sorted = mergeTwoListsRecursive(list1, list2);
        while (sorted != null) {
            System.out.printf(String.valueOf(sorted.val));
            sorted = sorted.next;
        }
    }
}
