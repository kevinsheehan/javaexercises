package org.kevin.structs;

/**
 * An implementation of a Stack using a singly linked list to store the data
 * @author Kevin
 */
public class Stack<T> {
    private LinkedListNode<T> top;

    public Stack() {}

    public void push(T node) {
        LinkedListNode<T> temp = top;
        top = new LinkedListNode<>(node);
        top.setNext(temp);
    }
    public T pop() {
        if (top == null) throw new NullPointerException();
        T temp = top.getData();
        top = top.getNext();
        return temp;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public static void main(String args[]) {
        Integer[] ints = { 0, 132, 14, 10, 5, 2, 68};
        Stack<LinkedListNode<Integer>> stack = new Stack<>();
        for (Integer i : ints) {
            stack.push(new LinkedListNode<>(i));
        }
        while (!stack.isEmpty()) {
            System.out.println(stack.pop().getData());
        }
    }
}
