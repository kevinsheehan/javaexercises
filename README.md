# Java Exercises #

This project was created while I was looking for a job. I used it to create solutions to general algorithms, data structures, and puzzles. This way I could brush up on my algorithm time/space complexities and rewind after sending out job applications.

### Examples ###
Feel free to browse the code and use any of it that you are interested in. These are by no means earth shattering implementations, but they are valid and mostly best case scenarios. Time complexity is explained for most implementations.