package org.kevin.probs;

import java.util.Arrays;

/**
 * @author Kevin
 */
public class Knapsack {
    private int[] values = {4, 5, 2, 7};
    private int[] weights = {1, 5, 4, 9};
    private int items = 4;
    private int capacity = 10;

    public void solve() {
        int[][] table = new int[values.length+1][capacity+1];
        for (int i = 0; i <= capacity; i++) {
            table[0][i] = 0;
        }
        for (int i = 1; i <= items; i++) {
            for (int j = 0; j <= capacity; j++) {
                if (j >= weights[i-1]) {
                    table[i][j] = Math.max(table[i-1][j], table[i-1][j-weights[i-1]] + values[i-1]);
                } else {
                    table[i][j] = table[i-1][j];
                }
            }
        }
        for (int[] row : table) {
            System.out.println(Arrays.toString(row));
        }
    }

    public static void main(String[] argv) {
        Knapsack ks = new Knapsack();
        ks.solve();
    }
}
