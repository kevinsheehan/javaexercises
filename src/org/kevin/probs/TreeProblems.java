package org.kevin.probs;

import org.kevin.structs.Stack;
import org.kevin.structs.trees.BinarySearchTree;
import org.kevin.structs.trees.BinaryTreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Kevin
 */
public class TreeProblems {
    /**
     * Time complexity is O(n) because each node is visited once
     * @param tree
     * @param <T>
     * @return
     */
    public static <T extends Comparable> int binaryTreeHeight(BinaryTreeNode<T> tree) {
        if (tree == null) return 0;
        return 1 + Math.max(binaryTreeHeight(tree.getLeft()), binaryTreeHeight(tree.getRight()));
    }

    /**
     * Time complexity is O(n) because each node is visited once
     * @param tree
     * @param <T>
     */
    public static <T extends Comparable> int preorderBinaryTreeTraversal(BinaryTreeNode<T> tree) {
        if (tree == null) return 0;
        System.out.println(tree.getData());
        int sum = 0;
        sum += preorderBinaryTreeTraversal(tree.getLeft());
        sum += preorderBinaryTreeTraversal(tree.getRight());
        return 1 + sum;
    }

    public static <T extends Comparable> ArrayList<BinaryTreeNode<T>> preorderBinaryTreeToArray(BinaryTreeNode<T> tree) {
        int size = preorderBinaryTreeTraversal(tree);
        ArrayList<BinaryTreeNode<T>> array = new ArrayList<>(size);

        Stack<BinaryTreeNode<T>> stack = new Stack<>();
        stack.push(tree);
        while (!stack.isEmpty()) {
            BinaryTreeNode<T> root = stack.pop();
            array.add(root);
            BinaryTreeNode<T> temp = root.getRight();
            if (temp != null) stack.push(temp);
            temp = root.getLeft();
            if (temp != null) stack.push(temp);
        }
        return array;
    }

    /**
     * Time complexity is O(n) because each node is visited once
     * Space complexity exists due to use of the stack and dynamic memory allocation
     * @param tree
     * @param <T>
     */
    public static <T extends Comparable> void preorderNoRecursion(BinaryTreeNode<T> tree) {
        Stack<BinaryTreeNode<T>> stack = new Stack<>();
        stack.push(tree);
        while (!stack.isEmpty()) {
            BinaryTreeNode<T> root = stack.pop();
            System.out.println(root.getData());
            BinaryTreeNode<T> temp = root.getRight();
            if (temp != null) stack.push(temp);
            temp = root.getLeft();
            if (temp != null) stack.push(temp);
        }
    }

    public static <T extends Comparable> void inorderBinaryTreeTraversal(BinaryTreeNode<T> tree) {
        if (tree == null) return;
        preorderBinaryTreeTraversal(tree.getLeft());
        System.out.println(tree.getData());
        preorderBinaryTreeTraversal(tree.getRight());
    }

    public static <T extends Comparable> void postorderBinaryTreeTraversal(BinaryTreeNode<T> tree) {
        if (tree == null) return;
        preorderBinaryTreeTraversal(tree.getLeft());
        preorderBinaryTreeTraversal(tree.getRight());
        System.out.println(tree.getData());
    }

    /**
     * Takes an unordered binary tree and converts it into a min heap. The problem required converting to an array and
     * then sorting and then using the sorted array to reconstruct the heap in a tree. This means the best time for
     * this solution is O(n log(n) due to sorting an Array.
     * @param tree
     * @param <T>
     * @return
     */
    public static <T extends Comparable> BinaryTreeNode<T> binaryTreeToHeap(BinaryTreeNode<T> tree) {
        // convert tree into array
        ArrayList<BinaryTreeNode<T>> array = preorderBinaryTreeToArray(tree);
        // sort the array
        Collections.sort(array, new Comparator<BinaryTreeNode<T>>() {
            @Override
            public int compare(BinaryTreeNode<T> node1, BinaryTreeNode<T> node2) {
                return node1.getData().compareTo(node2.getData());
            }
        });
        // recreate a tree using the now sorted array
        // heap will be made where the children of each node is located at position 2i + 1 and 2i + 2
        for (int i = 0; i < array.size(); i++) {
            int left = 2*i+1;
            int right = 2*i+2;
            array.get(i).setLeft(left >= array.size() ? null : array.get(left));
            array.get(i).setRight(right >= array.size() ? null : array.get(right));
        }
        return array.get(0);
    }

    /**
     * The time complexity of this algorithm is O(log(n)) because that is the time required to find any specific node
     * @param tree
     * @param value1
     * @param value2
     * @param <T>
     * @return
     */
    public static <T extends Comparable> T lowestCommonAncestor(BinarySearchTree<T> tree, T value1, T value2) {
        while (tree != null) {
            if (tree.getData().compareTo(value1) > 0 && tree.getData().compareTo(value2) > 0) {
                tree = (BinarySearchTree<T>)tree.getLeft();
            } else if (tree.getData().compareTo(value1) < 0 && tree.getData().compareTo(value2) < 0) {
                tree = (BinarySearchTree<T>)tree.getRight();
            } else {
                return tree.getData();
            }
        }
        return null;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public List<TreeNode> preorderBinaryTreeToArray(TreeNode tree) {
        ArrayList<TreeNode> array = new ArrayList<>();

        Stack<TreeNode> stack = new Stack<>();
        stack.push(tree);
        while (!stack.isEmpty()) {
            TreeNode root = stack.pop();
            array.add(root);
            TreeNode temp = root.right;
            if (temp != null) stack.push(temp);
            temp = root.left;
            if (temp != null) stack.push(temp);
        }
        return array;
    }

    public TreeNode invertTree(TreeNode root) {
        if (root == null) return null;

        invertTree(root.left);
        invertTree(root.right);

        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        return root;
    }

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        else if (root.val == sum && root.left == null && root.right == null) return true;

        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }

    /**
     * Values can be negative
     */
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        if (root == null) return new ArrayList<>();
        else if (root.val == sum && root.left == null && root.right == null) {
            List<Integer> path = new ArrayList<>();
            path.add(root.val);
            List<List<Integer>> paths = new ArrayList<>();
            paths.add(path);
            return paths;
        }

        List<List<Integer>> paths = new ArrayList<>();
        if (root.left != null) {
            List<List<Integer>> leftPaths = pathSum(root.left, sum - root.val);
            if (leftPaths != null) paths.addAll(leftPaths);
        }
        if (root.right != null) {
            List<List<Integer>> rightPaths = pathSum(root.right, sum - root.val);
            if (rightPaths != null) paths.addAll(rightPaths);
        }

        for (List<Integer> path : paths) {
            path.add(0, root.val);
        }

        return paths;
    }

    public static void main(String[] args) {
        //create tree
        /*BinaryTreeNode<Integer> tree = new BinaryTreeNode<>(null, null, null, 5);
        tree.setLeft(new BinaryTreeNode<>(tree, null, null, 3));
        tree.setRight(new BinaryTreeNode<>(tree, null, null, 10));
        tree.getLeft().setLeft(new BinaryTreeNode<>(tree.getLeft(), null, null, 1));
        tree.getLeft().setRight(new BinaryTreeNode<>(tree.getLeft(), null, null, 4));
        tree.getRight().setLeft(new BinaryTreeNode<>(tree.getRight(), null, null, 7));
        tree.getRight().setRight(new BinaryTreeNode<>(tree.getRight(), null, null, 12));
        // test tree height
        //System.out.println(TreeProblems.binaryTreeHeight(tree));
        // print out a preorder traversal of the tree without recursion
        TreeProblems.preorderNoRecursion(tree);*/

        /*BinarySearchTree<Integer> tree = new BinarySearchTree<>(null, null, null, 20);
        tree.insert(22);
        tree.insert(8);
        tree.insert(12);
        tree.insert(4);
        tree.insert(14);
        tree.insert(10);
        //System.out.println(TreeProblems.lowestCommonAncestor(tree, 10, 14));
        TreeProblems.inorderBinaryTreeTraversal(tree);*/


        TreeNode tree = new TreeNode(4);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(7);
        tree.left.left = new TreeNode(1);
        tree.left.right = new TreeNode(3);
        tree.right.left = new TreeNode(6);
        tree.right.right = new TreeNode(9);

        TreeProblems problems = new TreeProblems();
        for (List<Integer> path : problems.pathSum(tree, 7)) {
            for (Integer integer : path) {
                System.out.print(integer + ", ");
            }
            System.out.println();
        }
    }
}
