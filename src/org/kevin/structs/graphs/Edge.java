package org.kevin.structs.graphs;

/**
 * @author Kevin
 */
public class Edge<T> {
    private GraphNode node1;
    private GraphNode node2;

    private T value;

    public GraphNode getNode1() { return node1; }
    public GraphNode getNode2() { return node2; }
}
