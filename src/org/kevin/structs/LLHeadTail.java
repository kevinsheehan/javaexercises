package org.kevin.structs;

/**
 * Linked List implementation that tracks the head and tail nodes. Has insert and delete functions.
 * @author Kevin
 */
public class LLHeadTail<T> {
    private LinkedListNode<T> head;
    private LinkedListNode<T> tail;

    public LLHeadTail() {}

    public LinkedListNode<T> getElementAtIndex(int idx) {
        LinkedListNode<T> ret = head;
        for (int i = 0; i < idx; i++) {
            ret = ret.getNext();
        }
        return ret;
    }

    public boolean insertAfter(LinkedListNode<T> node, T data) {
        LinkedListNode<T> temp = head;
        if (node == null) {
            head = new LinkedListNode<>(data);
            if (temp == null)
                tail = head;
            else
                head.setNext(temp);
            return true;
        }
        if (node == head) {
            temp = head.getNext();
            LinkedListNode<T> next = head.setNext(new LinkedListNode<>(data));
            if (head != tail) {
                next.setNext(temp);
            } else {
                tail = next;
            }
            return true;
        }
        while (temp.getNext() != null) {
            temp = temp.getNext();
            if (temp == node) {
                LinkedListNode<T> temp2 = temp.getNext();
                LinkedListNode<T> next = temp.setNext(new LinkedListNode<>(data));
                if (temp == tail)
                    tail = next;
                else
                    next.setNext(temp2);
                return true;
            }
        }
        return false;
    }

    public boolean delete(LinkedListNode<T> node) {
        if (head == null) return false;
        if (node == head) {
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                head = head.getNext();
            }
            return true;
        }
        LinkedListNode<T> prev = head;
        while (prev != null) {
            if (prev.getNext() == node) {
                if (node == tail) {
                    tail = prev;
                    if (prev == head)
                        prev.setNext(prev);
                    else
                        prev.setNext(null);
                } else
                    prev.setNext(node.getNext());
                return true;
            }
            prev = prev.getNext();
        }
        return false;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public static void main(String args[]) {
        Integer[] ints = { 0, 132, 14, 10, 5, 2, 68};
        LLHeadTail<Integer> linkedList = new LLHeadTail<>();
        linkedList.insertAfter(null, ints[0]);
        System.out.println(linkedList.head.getData() + " - " + linkedList.tail.getData());
        for (int i = 1; i < ints.length; i++) {
            linkedList.insertAfter(null, ints[i]);
        }
        System.out.println(linkedList.head.getData() + " - " + linkedList.tail.getData());
    }
}
