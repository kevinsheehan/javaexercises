package org.kevin.probs;

import java.util.Arrays;

/**
 * @author Kevin
 */
public class Sorting {
    /**
     * Best and Average complexity of O(n*log(n)). Worst of O(n^2).
     * @param data
     * @return
     */
    public static int[] quickSortSimple(int[] data) {
        if (data.length < 2)
            return data;

        int pivotIdx = data.length/2;
        int pivotVal = data[pivotIdx];
        int leftCount = 0;

        for (int value : data) {
            if (value < pivotVal) leftCount++;
        }

        int[] left = new int[leftCount];
        int[] right = new int[data.length - leftCount - 1];

        int l = 0;
        int r = 0;

        for (int i = 0; i < data.length; i++) {
            if (i == pivotIdx) continue;
            int val = data[i];
            if (val < pivotVal) {
                left[l++] = val;
            } else {
                right[r++] = val;
            }
        }

        left = quickSortSimple(left);
        right = quickSortSimple(right);

        System.arraycopy(left, 0, data, 0, left.length);
        data[left.length] = pivotVal;
        System.arraycopy(right, 0, data, left.length+1, right.length);
        return data;
    }

    /**
     * Best, Average, and Worst case complexity of O(n*log(n))
     * @param data
     * @return
     */
    public static int[] mergeSortSimple(int[] data) {
        if (data.length < 2) {
            return data;
        }
        //Split data into 2 halves
        int middle = data.length/2;
        int[] left = new int[middle];
        int[] right = new int[data.length-middle];

        System.arraycopy(data, 0, left, 0, left.length);
        System.arraycopy(data, middle, right, 0, right.length);
        //Perform merge sort on each half
        left = mergeSortSimple(left);
        right = mergeSortSimple(right);
        //Merge and return the halves
        return merge(data, left, right);
    }

    public static int[] merge(int[] dest, int[] left, int[] right) {
        int destIdx = 0;
        int leftIdx = 0;
        int rightIdx = 0;

        while (leftIdx < left.length && rightIdx < right.length) {
            if (left[leftIdx] <= right[rightIdx]) {
                dest[destIdx++] = left[leftIdx++];
            } else {
                dest[destIdx++] = right[rightIdx++];
            }
        }
        // Populate the remaining values
        while (leftIdx < left.length) {
            dest[destIdx++] = left[leftIdx++];
        }
        while (rightIdx < right.length) {
            dest[destIdx++] = right[rightIdx++];
        }

        return dest;
    }

    public static void main(String[] argv) {
        int[] values = {0, 123, 35, 121, 62, 12, 64, 72, 83};
        System.out.println(Arrays.toString(quickSortSimple(values)));
    }
}
