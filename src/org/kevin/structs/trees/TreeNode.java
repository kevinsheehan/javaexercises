package org.kevin.structs.trees;

/**
 * @author Kevin
 */
public abstract class TreeNode<T extends Comparable> {
    private T data;
    private TreeNode<T> parent;
    private TreeNode<T>[] children;

    public TreeNode(TreeNode<T> parent, TreeNode<T>[] children, T data) {
        this.parent = parent;
        this.data = data;
        this.children = children;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public void setChildren(TreeNode<T>[] children) {
        this.children = children;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public TreeNode<T> getChildByIndex(int idx) {
        return children[idx];
    }

    public T getData() { return data; }
}
